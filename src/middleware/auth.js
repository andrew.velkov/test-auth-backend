import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';

import { JWT } from '../config/api';
import UserModel from '../models/UserModel';

module.exports = async (req, res, next) => {
  // get token from header
  const bearerHeader = req.header('Authorization');

  // check if not token
  if (!bearerHeader) {
    return res.status(401).json({
      msg: 'No token, authorization denied'
    });
  }

  // verify token
  try {
    const token = bearerHeader.replace('Bearer ', '');
    const decoded = jwt.verify(token, JWT.access.secret);
    if (decoded.type !== JWT.access.type) {
      return res.status(401).json({ errors: { msg: 'Unauthorized access!' } });
    }

    const isValidateUserId = mongoose.Types.ObjectId.isValid(decoded.userId);
    if (!isValidateUserId) {
      return res.status(400).json({
        error: { msg: 'Invalid user ID' },
      });
    };

    const isUserId = await UserModel.findOne({ _id: decoded.userId });
    if (!isUserId.id) {
      return res.status(401).json({ error: { msg: 'Such user does not exist' } });
    }

    req._userId = decoded.userId;
    next();
  } catch (err) {
    res.status(401).json({
      msg: 'Token is not valid'
    });
  }
}
