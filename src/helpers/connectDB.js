import mongoose from 'mongoose'

const url = 'mongodb+srv://username-node:4FyST82u5QHu2Wr6@node-rest-api-0cjur.mongodb.net/testdb';

const connectDB = async () => {
  try {
    await mongoose.connect(url, {
      keepAlive: true,
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      poolSize: 30,
      socketTimeoutMS: 360000,
      connectTimeoutMS: 360000,
    });
  } catch (err) {
    console.log('..connected error', err.message);
    process.on(1);
  }
}

module.exports = connectDB;
