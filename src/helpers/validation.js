import { check } from 'express-validator';

export const registrationSchema = [
  check('email', 'Email is required').not().isEmpty(),
  check('email', 'Please include a valid email').isEmail().isLength({
    max: 255,
  }),
  check('password', 'Please enter a title with 8 or more characters').isLength({
    min: 8,
    max: 255,
  }),
  check('first_name', 'First Name is required').not().isEmpty().isLength({
    max: 100,
  }),
  check('last_name', 'Last Name is required').not().isEmpty().isLength({
    max: 100,
  }),
];

export const loginSchema = [
  check('email', 'Error email or password').not().isEmpty().isEmail().bail(),
  check('password', 'Error email or password').not().isEmpty().exists().bail(),
];

export const refreshTokenSchema = [
  check('refresh_token', 'refreshToken is required').not().isEmpty().bail(),
];

export const AuthorSchema = [
  check('first_name', 'First Name error').not().isEmpty().isLength({
    min: 3,
    max: 100,
  }).bail(),
  check('last_name', 'Last Name error').isLength({
    max: 100,
  }),
  check('comments', 'Comments error').isLength({
    max: 255,
  }),
];
