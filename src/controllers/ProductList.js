import PRODUCTS from "../config/products";

class ProductList {
  // @route:  POST /api/products/order
  // @desc:   Send order
  addOrder = async (req, res) => {
    const { body: { order } } = req;
    if (order && order.length) {
      const data = {...req.body};
      return res.status(200).json({ result: 'Order sent successfully | test', data });
    } else {
      return res.status(400).json({ result: 'Error | test' });
    }
  }

  // @route:  GET /api/products
  // @desc:   Geting Product List
  getProductList = async (req, res) => {
    return res.status(200).json({ result: 'Success', data: PRODUCTS });
  }
}

export default ProductList;