import { validationResult } from 'express-validator';
import mongoose from 'mongoose';

import AuthorModel from '../models/AuthorModel';

// @access: Privat
class DomainsController {
  // @route:  POST /api/author
  // @desc:   Add new author
  addAuthor = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }

    try {
      const { _userId, body: { first_name, last_name, comments } } = req;
      const authorData = new AuthorModel({ _userId, first_name, last_name, comments });

      await authorData.save().then((data) => {
        return res.status(201).json({
          result: 'Added new author',
          data,
        });
      }).catch(err => {
        if (err) {
          return res.status(400).json({
            error: err.message,
          });
        }
      });
    } catch (err) {
      return res.status(500).json({ error: 'Error' });
    }
  }

  // @route:  GET /api/author
  // @desc:   Geting Author List
  getAuthorList = async (req, res) => {
    const { _userId } = req;
    await AuthorModel.find({ _userId }).select(['-__v']).exec((err, data) => {
      if (err) {
        return res.status(400).json({ error: 'Error' });
      }
      return res.status(200).json({ result: 'Success', data });
    });
  }

  // @route:  PUT /api/author/:authorId
  // @desc:   Updating Author By Id
  updateAuthorById = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }

    try {
      const { _userId, params: { authorId }, body: { first_name, last_name, comments } } = req;

      const isValidAuthorId = mongoose.Types.ObjectId.isValid(authorId);
      if (!isValidAuthorId) {
        return res.status(400).json({  error: 'Invalid ID' });
      };

      const authorData = await AuthorModel.findOne({ _userId, _id: authorId })
      if (!authorData) {
        return res.status(400).json({ error: 'Author is not found' });
      }

      const updatedData = {
        first_name, last_name, comments,
      };

      await AuthorModel.findOneAndUpdate({ '_id': authorId }, { $set: updatedData }, { new: true }, (err, data) => {
        if (err) {
          return res.status(400).json({ error: 'Error' });
        }
        console.log('data', data);
        return res.status(200).json({ result: 'Updated', data });
      });
    } catch (err) {
      return res.status(500).json({ error: 'Server Error' });
    }
  }

  // @route:  DELETE /api/author/:authorId
  // @desc:   Deleting Author By Id
  deleteAuthorById = async (req, res) => {
    try {
      const { _userId, params: { authorId } } = req;

      const isValidAuthorId = mongoose.Types.ObjectId.isValid(authorId);
      if (!isValidAuthorId) {
        return res.status(400).json({
          error: { msg: 'Invalid ID' }
        });
      };

      const authorData = await AuthorModel.findOne({ _userId, _id: authorId });
      if (!authorData) {
        return res.status(400).json({ error: 'No such id!' });
      }

      await AuthorModel.deleteOne({ _id: authorId }).then(data => {
        if (data) {
          return res.json({ result: 'Deleted' });
        } else {
          return res.json({ result: 'Error' });
        }
      });
    } catch (err) {
      return res.status(500).json({ error: { msg: 'Server Error' } });
    }
  }
}

export default DomainsController;