import { validationResult } from 'express-validator';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { pickBy, identity } from 'lodash';

import UserModel from '../models/UserModel';
import { JWT } from '../config/api';
import generateTokens from '../helpers/generateTokens';

class UserController {
  // @route  POST /api/user/register
  register = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        result: 'Error',
        error: errors.array(),
       });
    };

    const { email, password, first_name, last_name } = req.body;
    const salt = await bcrypt.genSalt(10);
    const userData = new UserModel({
      email,
      first_name,
      last_name,
      password: await bcrypt.hash(password, salt),
    });

    try {
      const getUserByEmail = await UserModel.findOne({ email });
      if (getUserByEmail) {
        return res.status(400).json({
          msg: 'User already exists'
        });
      }

      await userData.save().then(() => {
        const { accessToken, refreshToken } = generateTokens(userData.id);
        const data = {
          access_token: accessToken,
          refresh_token: refreshToken,
          result: 'User registered',
        };
        return res.status(201).json(data);
      }).catch((err) => err && res.json({ message: err.message }));
    } catch (err) {
      console.log('err', err.codeName);
      return res.status(500).json({ error: { msg: err.codeName } });
    }
  };

  // @route  POST /api/user/login
  login = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;
    try {
      const getUser = await UserModel.findOne({ email });
      if (!getUser) {
        return res.status(400).json({ errors: { msg: 'Invalid credentials' } });
      }

      const isMatch = await bcrypt.compare(password, getUser.password);
      if (!isMatch) {
        return res.status(400).json({ errors: { msg: 'Invalid Credentials' } });
      }

      const { accessToken, refreshToken } = generateTokens(getUser.id);
      const data = {
        access_token: accessToken,
        refresh_token: refreshToken,
        result: 'Authenticate success',
      };
      return res.status(200).json(data);
    } catch (err) {
      console.log('login:err', err.codeName);
      return res.status(500).json({ error: { msg: err.codeName } });
    }
  }

  // @route  POST /api/user/refresh
  // @desc   Regenerate accessToken
  refreshToken = async (req, res) => {
    try {
      const decoded = jwt.verify(req.body.refresh_token, JWT.refresh.secret);
      const getUser = await UserModel.findOne({ _id: decoded.userId });

      if (!getUser || decoded.userId !== getUser.id || decoded.type !== JWT.refresh.type) {
        return res.status(400).json({ errors: { msg: 'Invalid Credentials' } });
      }

      const { accessToken, refreshToken } = generateTokens(getUser.id);
      const data = {
        access_token: accessToken,
        refresh_token: refreshToken,
      };
      return res.json({ result: "Success", data });
    } catch (err) {
      return res.status(500).json({ error: { msg: 'Refresh token is not valid' } });
    }
  }

  // @route  GET /api/user/profile
  // @desc   Load Profile
  getUserProfile = async (req, res) => {
    try {
      await UserModel.findById(req._userId)
        .select(['-password', '-date', '-updatedAt', '-__v', '-reset_pass']).exec().then((user) => {
          return res.json({
            result: 'Success',
            data: user,
          })
        });
      } catch (err) {
        return res.status(500).json({ error: { msg: 'Error user info' } });
      }
  }
}

export default UserController;