import { Router } from 'express';

import AuthorController from '../controllers/AuthorController';
import auth from '../middleware/auth';
import { AuthorSchema } from '../helpers/validation';

const router = Router();
const author = new AuthorController();

router.post('/', [auth, AuthorSchema], author.addAuthor);
router.get('/', auth, author.getAuthorList);
router.put('/:authorId', [auth, AuthorSchema], author.updateAuthorById);
router.delete('/:authorId', auth, author.deleteAuthorById);

export default router;
