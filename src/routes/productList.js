import { Router } from 'express';

import ProductListController from '../controllers/ProductList';

const router = Router();
const productListController = new ProductListController();

router.get('/', productListController.getProductList);
router.post('/order', productListController.addOrder);

export default router;
