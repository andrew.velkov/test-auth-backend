import { Router } from 'express';

import UserController from '../controllers/UserController';
import auth from '../middleware/auth';
import { registrationSchema, loginSchema, refreshTokenSchema } from '../helpers/validation';

const router = Router();
const user = new UserController();

router.post('/register', registrationSchema, user.register);
router.post('/login', loginSchema, user.login);
router.post('/refresh', refreshTokenSchema, user.refreshToken);
router.get('/profile', auth, user.getUserProfile);

export default router;
