import mongoose, { Schema } from 'mongoose';

const AuthorSchema = new Schema({
  _userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
  },
  first_name: {
    type: String,
    min: 3,
    max: 255,
    required: true,
  },
  last_name: {
    type: String,
    max: 100,
  },
  comments: {
    type: String,
    max: 255,
  },
  // data: [],
}, {
  timestamps: true,
});

const AuthorModel = mongoose.model('authors', AuthorSchema);

export default AuthorModel;