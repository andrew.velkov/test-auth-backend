import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { PORT } from './config/api';
import connectDB from './helpers/connectDB';
import userRoute from './routes/user';
import authorRoute from './routes/author';
import productList from './routes/productList';

connectDB();
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.get('/', (req, res) => {
  res.send('Welcome!');
});

app.use('/api/author', authorRoute);
app.use('/api/user', userRoute);
app.use('/api/products', productList);

app.use((req, res, next) => {
  const error = new Error();
  
  next(err);
});

app.use((err, req, res, next) => {
  res.status(500).json({
    error: 'Oops, something broke!'
  });
});


app.listen(PORT, () => {
  console.log(`Server connected | ${PORT}`);
});
